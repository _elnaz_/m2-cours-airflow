# M2 - Cours Airflow

Supports de cours et matériel nécessaire pour la réalisation du travail demandé aux étudiants de Master 2 Data Science en Santé *(ILIS - 2023/2024)*.

***Chloé Saint-Dizier, Antoine Lamer*** - *10 mai 2024*