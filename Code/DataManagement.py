import pandas as pd 
import csv
import numpy as np


# Spécifiez le délimiteur approprié pour vos fichiers CSV
delim = ';'

# Lisez les fichiers CSV avec Pandas
medicaments = pd.read_csv('C:/Elnaz_DSS/M2/DataManagement/fichiers_data/medicament.csv', delimiter=delim)
patients = pd.read_csv('C:/Elnaz_DSS/M2/DataManagement/fichiers_data/patient.csv', delimiter=delim)
sejours = pd.read_csv('C:/Elnaz_DSS/M2/DataManagement/fichiers_data/sejour.csv', delimiter=delim)

# Affichez les premières lignes de chaque DataFrame pour vérifier s'ils ont été correctement lus
print("Medicaments:")
print(medicaments.head())

print("\nPatients:")
print(patients.head())

print("\nSejours:")
print(sejours.head())

print("Dimensions du DataFrame medicaments :", medicaments.shape)
print("Dimensions du DataFrame sejours :", sejours.shape)
print("Dimensions du DataFrame patients :", patients.shape)


# Supprimer les doublons
medicaments.drop_duplicates(inplace=True)
patients.drop_duplicates(inplace=True)
sejours.drop_duplicates(inplace=True)


# Supprimer les lignes avec des valeurs manquantes dans chaque DataFrame
#medicaments.dropna(inplace=True)
#patients.dropna(inplace=True)
#sejours.dropna(inplace=True)

# Supprimer les lignes où toutes les valeurs sont manquantes
medicaments.dropna(how='all', inplace=True)
patients.dropna(how='all', inplace=True)
sejours.dropna(how='all', inplace=True)


# SEJOURS
# Utiliser l'encodage one-hot pour les variables catégoriques dans le DataFrame patients
#patients = pd.get_dummies(patients, columns=['SEXE'])

# Normaliser les valeurs dans la colonne 'DATE_INTERVENTION' dans le DataFrame sejours
sejours['DATE_INTERVENTION'] = pd.to_datetime(sejours['DATE_INTERVENTION'])

# Convertir les colonnes HEURE_DEBUT et HEURE_FIN en objets DateTime
sejours['HEURE_DEBUT'] = pd.to_datetime(sejours['HEURE_DEBUT'])
sejours['HEURE_FIN'] = pd.to_datetime(sejours['HEURE_FIN'])
# Formater les valeurs dans les colonnes HEURE_DEBUT et HEURE_FIN en heure seulement (HH:MM:SS)
sejours['HEURE_DEBUT'] = sejours['HEURE_DEBUT'].dt.strftime('%H:%M:%S')
sejours['HEURE_FIN'] = sejours['HEURE_FIN'].dt.strftime('%H:%M:%S')

print("\nSejours:")
print(sejours.head(10))
# vérifier qu'il n'y a pas de valeurs aberrantes dans le DataFrame sejours
sejours.describe()


print("\nPatients:")
print(patients.head())


# MEDICAMENTS
# Supprimer les caractères spéciaux dans toutes les colonnes du DataFrame medicaments
special_chars = r'[?:;,.!@#\$%\^&\*\(\)\[\]{}<>]'

for col in medicaments.columns:
    if medicaments[col].dtype == object:  # Appliquer seulement aux colonnes de type texte
        medicaments[col] = medicaments[col].str.replace(special_chars, '', regex=True)


print("\nMedicaments:")
print(medicaments.head())



