from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from datetime import datetime, timedelta
import os


# Fonction pour exécuter le script Python de DataManagement
def run_data_management_script():
    os.system('python C:/Elnaz_DSS/M2/DataManagement/airflow/dags/DataManagement/DataManagement.py')

# Fonction pour compiler les données
def compile_data():
    from data_compilation import compile_data as compile
    compile()

with DAG(
    'data_management_dag',
    description='DAG pour exécuter le script de DataManagement et compiler les données mensuelles',
    schedule_interval='@monthly',
    start_date=datetime(2024, 5, 9),
    catchup=False
) as dag:

    t1 = BashOperator(
        task_id='lister_contenu',
        bash_command='python3 /opt/airflow/dags/DataManagement/DataManagement.py'
    )
    
    t2 = PythonOperator(
        task_id='executer_data_management',
        python_callable=run_data_management_script
    )
    
    t3 = PythonOperator(
        task_id='compiler_donnees',
        python_callable=compile_data
    )

    t1 >> t2 >> t3



    
